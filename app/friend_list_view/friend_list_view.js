'use strict';

angular.module('myApp.friend_list_view', ['ngRoute' , 'toaster' , 'ngAnimate', 'ngLocationUpdate']).config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/', {
		templateUrl : 'friend_list_view/friend_list_view.html',
		controller : 'FriendListViewCtrl'
	});
}]).controller('FriendListViewCtrl', ['$scope', 'services', '$location', '$route' , 'toaster', function($scope, services, $location, $route, toaster) {

 	$scope.pop = function(){
            toaster.pop('error', "Person Deleted", "You have successfully deleted a person");
    };

	$scope.getList = function() {
		services.getPeople().then(function(data) {
			$scope.people = data.data;
		});
	};
	$scope.getList();
	
	$scope.deletePerson = function(id) {
			services.deletePerson(id).then(function (response) { $scope.getList(); });
			$scope.pop();
	};
}]);




/* This is what I was using to try connect to API... Didn't want to work... Think it was a CORS issue..

 $http({
 method: 'GET',
 url: 'https://api.floatschedule.com/api/v1/',
 headers: {
 'Authorization': 'Bearer 325dc0a457449671ef723e8da9f235ee3d1b8407',
 'Content-Type': 'application/x-www-form-urlencoded',
 'Accept': 'application/json'
 }

 }).then(function successCallback(response) {
 $scope.names = response.records;
 // success
 }, function errorCallback(response) {
 // errors
 });

 */

