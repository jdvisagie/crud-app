'use strict';

angular.module('myApp.create_friend', ['ngRoute' , 'toaster' , 'ngAnimate']).config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/create_friend/0', {
		templateUrl : 'create_friend/create_friend.html',
		controller : 'CreateFriendCtrl'

	});
}]).controller('CreateFriendCtrl', ['$scope', '$rootScope', '$location', 'services' , 'toaster' , function($scope, $rootScope, $location, services, toaster) {
	
  	$scope.confirm = function(){
            toaster.pop('success', "New Person Added", "You have successfully added a new person");
    };
 
	$scope.savePerson = function(person) {
		services.insertPerson(person);
     	$location.path('/');
     	
        
	};
	
}]); 