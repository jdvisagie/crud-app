'use strict';

angular.module('myApp.friend_view', ['ngRoute', 'ngTagsInput' , 'toaster' , 'ngAnimate']).config(['$routeProvider', function($routeProvider) {
	$routeProvider.when('/friend_view/:personID', {
		templateUrl : 'friend_view/friend_view.html',
		controller : 'FriendViewCtrl',
		resolve : {
			person : function(services, $route) {
				var personID = $route.current.params.personID;
				return services.getPerson(personID);
			}
		}
	});

}]).controller('FriendViewCtrl', ['$scope', '$rootScope', '$location', '$routeParams', 'services', 'person', '$route' , 'toaster', function($scope, $rootScope, $location, $routeParams, services, person, $route, toaster) {
	$scope.person = person.data[0];
	$scope.skills = $scope.person.skills;
	
	  	$scope.updatedpop = function(){
            toaster.pop('success', "Updated Person Details", "You have successfully updated the person\'s details");
    };
    	$scope.deletedpop = function(){
            toaster.pop('error', "Person Deleted", "You have successfully updated the person\'s details");
    };


	$scope.deletePerson = function(person) {
			services.deletePerson(person.id);
			$scope.deletedpop();
			$location.path('/');
	};

	$scope.savePerson = function(person) {
		services.updatePerson(person);
		$scope.updatedpop();
		$location.path('/');
	};
}]);
