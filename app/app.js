'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', ['ngRoute', 'myApp.friend_list_view', 'myApp.create_friend', 'myApp.friend_view', 'myApp.version'])

.config(['$routeProvider', function($routeProvider) {
	$routeProvider.otherwise({
		redirectTo : '/'
	});
}])
.factory("services", ['$http', function($http) {
	var serviceBase = 'http://localhost:3000/';
	var obj = {};

	obj.getPeople = function() {
		var result = $http.get(serviceBase + 'people', {headers: { 'Cache-Control' : 'no-cache'}});
		return result;
	};

	obj.getPerson = function(personID) {
		return $http.get(serviceBase + 'people?id=' + personID);
	};

	obj.insertPerson = function(person) {
		// Just setting some default value to keep DB key-value pairs consistant, this is only for this test - not pretty ;(
		person.avatar_file = "https://i1.wp.com/4f924a82f1b7a7989f8c-8c58e9ee3a00416a4085586f251e9af6.ssl.cf2.rackcdn.com/avatar-kale.png?ssl=1";
		person.job_title = null;
		person.department = {};
		person.im = null;
		person.wk_day_hrs = null;
		person.non_wk_days = null;
		person.employee_type = 1;
		person.contractor = 1;
		person.access_rights = 0;
		person.access_id = 0;
		//person.skills = null;
		person.department_filter_id = null;
		var today = new Date();
		person.created = today.toISOString().substring(0, 10);
		person.active = 1;
		// End of values
		var  result = $http.post(serviceBase + 'people', person).then(function(result) {
			return result;
		});
	};

	obj.updatePerson = function(person) {
		var  result = $http.post(serviceBase + 'people', person).then(function(status) {
			return result;
		});
	};

	obj.deletePerson = function(id) {
		var result = $http.delete(serviceBase + 'people/' + id);
		return result;
	};
	return obj;
}])
.controller('NavigationCtrl', ['$scope', '$location' ,  function($scope, $location) {
	$scope.isCurrentPath = function(path) {
		return $location.path() == path;
	};

}]);
