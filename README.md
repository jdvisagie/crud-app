# Mobile Guardian � AngularJS test


### Prerequisites

We use a number of node.js tools to initialize and test angular-seed. You must have node.js and
its package manager (npm) installed.  You can get them from [http://nodejs.org/](http://nodejs.org/).

### Install Dependencies

We have two kinds of dependencies in this project: tools and angular framework code.  The tools help
us manage and test the application.

* We get the tools we depend upon via `npm`, the [node package manager][npm].
* We get the angular code via `bower`, a [client-side code package manager][bower].

We have preconfigured `npm` to automatically run `bower` so we can simply do:

```
sudo npm install
```

Behind the scenes this will also call `bower install`.  You should find that you have two new
folders in your project.

* `node_modules` - contains the npm packages for the tools we need
* `app/bower_components` - contains the angular framework files

*Note that the `bower_components` folder would normally be installed in the root folder but
angular-seed changes this location through the `.bowerrc` file.  Putting it in the app folder makes
it easier to serve the files by a webserver.*

# Run the Application

The Float Schedule API has been sporadic in the way it is dealing with CORS requests, so the decision was taken to run a JSON server locally.

```
cd /path/app/asset
json-server --watch db.json
```

The simplest way to view the application is to start the local dev web server by running:

```
npm start
```

Now browse to the app at `http://localhost:8000/app/index.html`.